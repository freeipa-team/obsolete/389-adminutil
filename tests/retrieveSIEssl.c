/*
 * BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libadminutil/distadm.h"
#include "libadminutil/admutil.h"
#include "libadminutil/srvutil.h"
#include "libadmsslutil/admsslutil.h"
#include "libadmsslutil/srvutilssl.h"
#include "libadmsslutil/psetcssl.h"

#include "nss.h"
#include "ssl.h"

int
main(int ac, char **av)
{
    int _ai=ADMUTIL_Init();
    AdmldapInfo ldapInfo = NULL;
    AttrNameList nameList = NULL;
    AttributeList isieList = NULL;
    char *configdir = NULL;
    char *securitydir = NULL;
    int rval = 0;
    char **p;
    int siecnt = 0;
    char *host = NULL;
    char *siedn = NULL;
    char buf[BUFSIZ];
    char *userdn;
    char *passwd;

    (void)_ai;
    if (ac > 1) {
        configdir = strdup(*(av+1));
    }
    
    if (NULL == configdir || 0 == strlen(configdir)) {
        fprintf(stderr, "ERROR: config dir was not specified.\n");
        exit(1);
    } else {
        fprintf(stdout, "%s: configdir: %s\n", *av, configdir);
    }

    if (ac > 2) {
        securitydir = strdup(*(av+2));
    }
    
    if (NULL == securitydir || 0 == strlen(securitydir)) {
        fprintf(stderr, "ERROR: security dir was not specified.\n");
        exit(1);
    } else {
        fprintf(stdout, "%s: securitydir: %s\n", *av, securitydir);
    }

    rval = ADMSSL_InitSimple(configdir, securitydir, 0);
    if (rval) {
        fprintf(stderr, "ADMSSL_InitSimple(%s,%s) failed: %d\n", configdir, securitydir, rval);
        exit(1);
    }

    /*
     * get the LDAP information from admin server config info
     */
    ldapInfo = admldapBuildInfo(configdir, &rval);
    if (NULL == ldapInfo) {
        fprintf(stderr, "ERROR: empty ldapInfo\n");
        rval = 1;
        goto done;
    }

    siedn = admldapGetSIEDN(ldapInfo);
    ADM_GetUserDNString(&rval, &userdn);
    ADM_GetCurrentPassword(&rval, &passwd);

    admldapSetSIEDN(ldapInfo, userdn);
    admSetCachedSIEPWD(passwd);
    /*
     * get servers' DNs belonging to the Admin Server
     */
    nameList = getServerDNListSSL(ldapInfo);
    if (NULL == nameList) {
        fprintf(stderr, "ERROR: empty nameList\n");
        rval = 1;
        goto done;
    }

    admldapSetSIEDN(ldapInfo, siedn);

    host = admldapGetHost(ldapInfo);
    for (p = nameList; p && *p; p++) {
        PsetHndl pset = NULL;
        fprintf(stdout, "%s\n", *p);
        if (0 == strncasecmp(*p, "cn=", 3)) {
            siecnt++;
        }
        /* Create Pset for each individual server */
        rval = 0;
        pset = psetRealCreateSSL(ldapInfo, host,
                                 admldapGetPort(ldapInfo),
                                 admldapGetSecurity(ldapInfo),
                                 *p,
                                 userdn,
                                 passwd,
                                 NULL,
                                 &rval);
        if (rval && (rval != PSET_LOCAL_OPEN_FAIL)) {
            fprintf(stderr, "Error creating pset for server [%s] [%d: %s]\n", *p,
                    rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
        }
        if (pset) {
            char*  serverid = psetGetAttrSingleValue(pset, "nsServerID", &rval);

            fprintf(stderr, "psetGetAttrSingleValue(nsServerID) returned [%d: %s]\n",
                    rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));

            if (serverid) {
                fprintf(stderr, "Found server id [%s] for server DN [%s]\n",
                        serverid, *p);
            } else {
                fprintf(stderr, "Could not find id for server DN [%s]\n", *p);
            }

            PL_strfree(serverid);
        }
        psetDelete(pset);
    }
    deleteAttrNameList(nameList);
    nameList = NULL;
    if (siecnt > 0) {
        fprintf(stdout, "SUCCESS: SIE%s retrieved\n",  siecnt>1?"S are":" is");
    } else {
        fprintf(stdout, "FAILURE: SIE is not retrieved\n");
    }
    PL_strfree(host);

    /*
     * get servers' DNs belonging to the Admin Server
     */
    admldapSetSIEDN(ldapInfo, userdn);
    isieList = getInstalledServerDNListSSL(ldapInfo);
    admldapSetSIEDN(ldapInfo, siedn);
    if (NULL == isieList) {
        fprintf(stderr, "ERROR: empty isieList\n");
        rval = 1;
        goto done;
    }

    for (siecnt = 0; isieList[siecnt]; siecnt++) {
        fprintf(stdout, "productID = %s\n", isieList[siecnt]->attrName);
        fprintf(stdout, "productDN = %s\n", isieList[siecnt]->attrVal[0]);
    }
    deleteAttributeList(isieList);
    if (siecnt > 0) {
        fprintf(stdout, "SUCCESS: ISIE%s retrieved\n",  siecnt>1?"S are":" is");
    } else {
        fprintf(stdout, "FAILURE: ISIE is not retrieved\n");
    }
done:
    destroyAdmldap(ldapInfo);
    SSL_ClearSessionCache();
    NSS_Shutdown();
    PR_Cleanup();
    exit(rval);
}
