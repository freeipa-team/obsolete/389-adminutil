#
# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation version
# 2.1 of the License.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# END COPYRIGHT BLOCK
#

In order to run the test cases, you must have a directory server installed.
The test prefers Fedora DS 1.1 or later, but it could be made to use
others with minor modifications.  The test script will create an instance
of the server to use for testing, and it will load the nsroot.ldif data
set for use.

If you want to run the *ssl tests, you must have ssl enabled on the server.
Edit the setup.sh script to uncomment usessl=1.  You also have to edit
secport to the correct port number.  Finally, edit secdir to point to
your key/cert database directory.  One convenience would be for the test
script to configure the server for ssl, or at least just allow you to pass
in the CA cert used to issue the server cert, and just create the key/cert
databases in the test directory.  But for now, just point the secdir at
the directory server key/cert db directory.

To run:
First, make the tests.  They do not get built by default.  You can use make check-TESTS - this will build the tests and attempt to run them, which will fail because the setup.sh script must be used to run the tests.  If you want to make the tests manually, do
 make retrieveSIE retrieveSIEssl psetread psetreadssl psetwrite
Next, setup expects to be able to run from the directory you built in, in order to use libtool to run the tests.
Next, run setup like this:
 /path/to/adminutil/tests/setup.sh /path/to/adminutil/tests
You can also run the tests with gdb or valgrind by editing setup.sh

The tests (so far):
retrieveSIE prints a list of the DNs of the server instances (slapd-localhost, slapd-localhost2, and admin server)
retrieveSIEssl simulates the sync_task_sie_data() function in mod_admserv
psetread uses the pset api to read an entry
psetreadssl is the same as psetread but uses ssl
psetwrite uses the pset api to modify an entry
