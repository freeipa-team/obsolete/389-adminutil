/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/*
 * psetc.h
 *
 * $Id: psetc.h,v 1.3 2007/04/04 19:37:41 rmeggins Exp $
 */

#ifndef __PSETC_H__
#define __PSETC_H__

#include <libadminutil/admutil.h>

#ifdef __cplusplus
extern "C" {
#endif

/* The following are the data structure and public methods for pset */

typedef void*    PsetHndl;

#define PSET_OP_OK 0
#define PSET_OP_FAIL 1
#define PSET_SYSTEM_ERR 2
#define PSET_ENV_ERR 3
#define PSET_ARGS_ERROR 4
#define PSET_NULL_HANDLE 5
#define PSET_LOCAL_MODE 6
#define PSET_LOCAL_OPEN_FAIL 7
#define PSET_AUTH_FAIL 8
#define PSET_ACCESS_FAIL 9
#define PSET_ENTRY_NOT_EXIST 10
#define PSET_ATTR_NOT_EXIST 11
#define PSET_ENTRY_EXIST 12
#define PSET_ATTR_EXIST 13
#define PSET_NOT_ENTRY 14
#define PSET_NOT_ATTR 15
#define PSET_NO_LDAP_HNDL 16
#define PSET_NO_DN 17
#define PSET_NO_DATA 18
#define PSET_NO_VALUE 19
#define PSET_NO_PARENT 20
#define PSET_PARTIAL_GET 21
#define PSET_PARTIAL_OP  22
#define PSET_ILLEGAL_OP 23
#define PSET_NOT_IMPLEMENT 24
#define PSET_ATTR_NOT_ALLOWED 25


/*
   Most the Pset operation are atomic!!! Only psetGetAttrList may return
   partial result.
   */

/* Init the connection to LDAP server,
   Get the values from LDAP server
   sync with the local file if inconsistent occurs
   if LDAP is not available, use local file, and in READ-ONLY mode

   Arguments -
   server_id: id for server instance
   configRoot: Root for config file, if NULL, use NETSITE_ROOT env variable
   operationInfo: operation information

   Return -
   Handle for pset (used for future operation)
   */

PR_IMPLEMENT(PsetHndl) 
psetCreate(char* serverID, char* configRoot, char* userDN, char* passwd,
	   int* errorcode);

PR_IMPLEMENT(PsetHndl)
psetRealCreate(AdmldapInfo ldapInfo, char* ldapHost, int ldapPort, char* sieDN, char* userDN,
	       char* passwd, char* cacheFile, int* errorcode);

PR_IMPLEMENT(PsetHndl)
psetFullCreate(struct ldap *ld, char* sieDN, char* cacheFile, int ldunbindFlag,
	       int* errorcode);

PR_IMPLEMENT(PsetHndl)
psetFullCreateRef(struct ldap *ld, char* sieDN, char* cacheFile, 
		  int ldunbindFlag, int* errorcode, char *userDN, 
		  char *passwd);

PR_IMPLEMENT(PsetHndl)
psetFileCreate(char* configFile, char* filter, int* errorcode);

PR_IMPLEMENT(PsetHndl)
psetRealLDAPImport(AdmldapInfo ldapInfo, PsetHndl pset, char* ldapHost, int ldapPort, char* sieDN, char* userDN,
	       char* passwd, char* cacheFile, char* filter, int* errorcode);

PR_IMPLEMENT(PsetHndl)
psetFullLDAPImport(PsetHndl pset, struct ldap *ld, char* sieDN, char* cacheFile, char* filter, int ldunbindFlag, int* errorcode);

PR_IMPLEMENT(PsetHndl)
psetFullLDAPImportRef(PsetHndl pset, struct ldap *ld, char* sieDN, char* cacheFile, char* filter, int ldunbindFlag, int* errorcode, char *userDN, char *passwd);

PR_IMPLEMENT(PsetHndl)
psetFileImport(PsetHndl pset, char* configFile, char* filter, int rw, int* errorcode);

/* 
   Close the local file
   Memory cleanup
   */
PR_IMPLEMENT(int)
psetDelete(PsetHndl pseth);

/* Check the existence of the attribute name, it can be LDAP entry or LDAP
   attribute */
PR_IMPLEMENT(int)
psetCheckAttribute(PsetHndl pseth, NameType name);

/* Retrieve the value of the atrribute based on the given attribute name */
PR_IMPLEMENT(ValueType)
psetGetAttrValue(PsetHndl pseth, NameType name, int* errorcode);

/* Retrieve the first value of the atrribute based on the given attribute name */
PR_IMPLEMENT(char*)
psetGetAttrSingleValue(PsetHndl pseth, NameType name, int* errorcode);

/* Retrieve the values of the atrributes based on the given attribute name list */
PR_IMPLEMENT(AttributeList)
psetGetAttrList(PsetHndl pseth, AttrNameList nl, int* errorcode);

/* Retrieve the values of the all atrributes of one LDAP entry */
PR_IMPLEMENT(AttributeList)
psetGetAllAttrs(PsetHndl pseth, NameType attrName, int* errorcode);

/* Retrieve the values of the all atrributes of one LDAP entry, including ACI */
PR_IMPLEMENT(AttributeList)
psetGetAllAttrsACI(PsetHndl pseth, NameType attrName, int* errorcode);

/* Retrieve the name of the all children */
PR_IMPLEMENT(AttrNameList)
psetGetChildren(PsetHndl pseth, NameType attrName, int* errorcode);

/* Retrieve the object type of one LDAP entry */
PR_IMPLEMENT(ValueType)
psetGetObjectClass(PsetHndl pseth, NameType name, int* errorcode);

/* Set the value of given attribute */
PR_IMPLEMENT(int)
psetSetAttr(PsetHndl pseth, NameType name, ValueType val);

/* Set the value of given single-valued attribute */
PR_IMPLEMENT(int)
psetSetSingleValueAttr(PsetHndl pseth, NameType name, char*  val);

/* Set the value of given attribute list */
PR_IMPLEMENT(int)
psetSetAttrList(PsetHndl pseth, AttributeList nvl);

/* These functions can dynamically create/delete attributes from LDAP server,
   Not quite sure the usefulness of these functions  */

/* Add an attribute to the pset entry */
PR_IMPLEMENT(int)
psetAddAttribute(PsetHndl pseth, NameType name, ValueType val);

/* Add a single valued attribute to the pset entry */
PR_IMPLEMENT(int)
psetAddSingleValueAttribute(PsetHndl pseth, NameType name, char* val);

/* Add a list of attributes to the pset entry */
PR_IMPLEMENT(int)
psetAddAttrList(PsetHndl pseth, AttributeList attrList);

/* Remove an attribute from pset entry */
PR_IMPLEMENT(int)
psetDeleteteAttribute(PsetHndl pseth, NameType attrName);

/* Remove a list of attributes from pset entry */
PR_IMPLEMENT(int)
psetDeleteteAttrList(PsetHndl pseth, AttrNameList nl);

/* Add a LDAP node  */
PR_IMPLEMENT(int)
psetAddEntry(PsetHndl pseth, NameType parent, NameType name,
	     AttrNameList objectclasses, AttributeList initList);

/* Delete a LDAP entry  */
PR_IMPLEMENT(int)
psetDeleteEntry(PsetHndl pseth, NameType name);

/* Generate pset error string by given pset error number
   If buffer is NULL, the return value will be allocated by malloc and
   must be free'd by the caller.  Even in an error condition, if
   buffer is NULL, a malloc'd empty string will be returned.  The
   value will be returned in buffer, if given.  buffer will be
   properly NULL terminated, even if bufsize is not large enough to
   accomodate the entire string (i.e. it's truncated).  Buffer will
   always be NULL terminated, so that even if an error occurred,
   buffer will be initialized to an empty string, so you do not have
   to worry about initializing it first.  If buffer is given, the
   return value will point to buffer, so that you can use the return
   value directly:
   char buf[BUFSIZE];
   int rc = 0;
   ...
   fprintf(stderr, "Error: %s\n",
           psetErrorString(num, lang, buf, sizeof(buf), &rc));

   The rc parameter may be used to determine if there was an overflow
   condition or some other error.  If rc == 0, the operation was
   successful.  If rc == 1, an overflow occurred - the given buffer
   was too small to hold the contents.  If rc == -1 or some other
   value, some other error occurred,
 */
PR_IMPLEMENT(char*)
psetErrorString(int errorNum, char* lang, char *buffer, size_t bufsize, int *rc);

/* Setting up LDAP referal */
PR_IMPLEMENT(int)
psetSetLDAPReferalInfo(PsetHndl pseth, char* userDN, char* passwd);

/* Replicate SIE tree from source to dest */
PR_IMPLEMENT(int)
psetReplicateSIE(PsetHndl source, PsetHndl dest, char *source_groupDN, char *dest_groupDN);

#ifdef __cplusplus
}
#endif

#endif /* __PSETC_H__ */

