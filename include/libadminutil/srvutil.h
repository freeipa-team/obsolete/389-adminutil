/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/*
 * srvutil.h
 *
 * $Id: srvutil.h,v 1.4 2007/05/08 19:13:25 rmeggins Exp $
 */

#ifndef __SRVUTIL_H__
#define __SRVUTIL_H__

#include <libadminutil/psetc.h>

#ifdef __cplusplus
extern "C" {
#endif

PR_IMPLEMENT(AttrNameList)
retrieveSIEs(PsetHndl domainPset, char* domainDN, char *adminName);

PR_IMPLEMENT(AttrNameList)
getServerDNList(AdmldapInfo info);

PR_IMPLEMENT(AttributeList)
retrieveISIEs(PsetHndl domainPset, char* domainDN);

PR_IMPLEMENT(AttributeList)
getInstalledServerDNList(AdmldapInfo info);

/* return the SIE DN for the given server */
PR_IMPLEMENT(char *)
findSIEDNByID(AdmldapInfo info, const char *serverID);

#ifdef __cplusplus
}
#endif

#endif /* __SRVUTIL_H__ */
