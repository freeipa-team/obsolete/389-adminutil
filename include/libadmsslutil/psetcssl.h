/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/*
 * psetcssl.h
 *
 * $Id: psetcssl.h,v 1.2 2005/12/06 18:38:37 nhosoi Exp $
 */

#ifndef __PSETCSSL_H__
#define __PSETCSSL_H__

#include "libadminutil/psetc.h"

#ifdef __cplusplus
extern "C" {
#endif

PR_IMPLEMENT(PsetHndl) 
psetCreateSSL(char* serverID, char* configRoot, char* userDN, char* passwd,
	      int* errorcode);

PR_IMPLEMENT(PsetHndl)
psetRealCreateSSL(AdmldapInfo ldapInfo, char* ldapHost, int ldapPort, int secure, char* sieDN,
		  char* userDN, char* passwd, char* configFile,
		  int* errorcode);

PR_IMPLEMENT(PsetHndl)
psetRealLDAPImportSSL(AdmldapInfo ldapInfo, PsetHndl pseth, char* ldapHost, int ldapPort,
		      int secure, char* sieDN, char* userDN, char* passwd, 
		      char* configFile, char* filter, int* errorcode);
#ifdef __cplusplus
}
#endif

#endif /* __PSETCSSL_H__ */
