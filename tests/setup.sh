#!/bin/sh

testdir="$1"
hostname=vmhost.testdomain.com
domain=testdomain.com
sroot=/home/$USER/dsol
port=1200
secport=1201
rootdn="cn=directory manager"
escapedrootdn='cn\\\\3Ddirectory manager'
rootpw=password
adminpw=boguspassword
adminpw=admin
#needinstance=1
#needdata=1
usessl=1
secdir=/home/$USER/save
#PATH=/usr/lib64/mozldap:$PATH
#export PATH
instance=ds

if [ "$needinstance" ] ; then
$sroot/sbin/setup-ds.pl -s -f - <<EOF
[General]
FullMachineName=   $hostname
SuiteSpotUserID=   $USER
[slapd]
ServerPort=   $port
ServerIdentifier=   $instance
Suffix=   o=NetscapeRoot
RootDN=   $rootdn
RootDNPwd=  $rootpw
EOF

sslconf=/tmp/sslconf.$$.ldif
cat > $sslconf <<EOF
dn: cn=encryption,cn=config
changetype: modify
replace: nsSSL3
nsSSL3: on
-
replace: nsSSLClientAuth
nsSSLClientAuth: allowed
-
add: nsSSL3Ciphers
nsSSL3Ciphers: -rsa_null_md5,+rsa_rc4_128_md5,+rsa_rc4_40_md5,+rsa_rc2_40_md5,
 +rsa_des_sha,+rsa_fips_des_sha,+rsa_3des_sha,+rsa_fips_3des_sha,+fortezza,
 +fortezza_rc4_128_sha,+fortezza_null,+tls_rsa_export1024_with_rc4_56_sha,
 +tls_rsa_export1024_with_des_cbc_sha

dn: cn=config
changetype: modify
add: nsslapd-security
nsslapd-security: on
-
replace: nsslapd-ssl-check-hostname
nsslapd-ssl-check-hostname: off
-
replace: nsslapd-secureport
nsslapd-secureport: $secport

dn: cn=RSA,cn=encryption,cn=config
changetype: add
objectclass: top
objectclass: nsEncryptionModule
cn: RSA
nsSSLPersonalitySSL: Server-Cert
nsSSLToken: internal (software)
nsSSLActivation: on

EOF

ldapmodify -x -h $hostname -p $port -D "$rootdn" -w "$rootpw" -c -f $sslconf
rm -f $sslconf

$sroot/lib/dirsrv/slapd-$instance/stop-slapd
cp $secdir/*.db $sroot/etc/dirsrv/slapd-$instance
cp $secdir/pin.txt $sroot/etc/dirsrv/slapd-$instance
$sroot/lib/dirsrv/slapd-$instance/start-slapd

fi

if [ "$needdata" ] ; then
    for file in $testdir/*.ldif.tmpl ; do
        sed \
            -e "s/%as_uid%/admin/g" \
            -e "s/%as_passwd%/admin/g" \
            -e "s/%domain%/$domain/g" \
            -e "s/%console_version%/0.0/g" \
            -e "s/%as_baseversion%/0.0/g" \
            -e "s/%ds_console_jar%/389-ds.jar/g" \
            -e "s/%fqdn%/$hostname/g" \
            -e "s/%ds_port%/$port/g" \
            -e "s/%ds_secure_port%/$secport/g" \
            -e "s/%ds_suffix%/$suffix/g" \
            -e "s/%ds_user%/$USER/g" \
            -e "s/%brand%/389/g" \
            -e "s/%dsid%/$instance/g" \
            -e "s,%uname_a%,`uname -a`,g" \
            -e "s/%uname_m%/`uname -m`/g" \
            -e "s/%ds_version%/0.0/g" \
            -e "s/%ds_buildnum%/0.0/g" \
            -e "s/%asid%/$instance/g" \
            -e "s/%vendor%/389/g" \
            -e "s/%timestamp%/`date`/g" \
            -e "s/%rootdn%/$rootdn/g" \
            -e "s/%escapedrootdn%/$escapedrootdn/g" \
            -e "s/%ds_sie%/cn=slapd-$instance,cn=389 Directory Server,cn=Server Group,cn=$hostname,ou=$domain,o=NetscapeRoot/g" \
            -e "s/%as_sie%/cn=admin-serv-$instance,cn=389 Administration Server,cn=Server Group,cn=$hostname,ou=$domain,o=NetscapeRoot/g" \
            -e "s/%as_version%/0.0/g" \
            -e "s/%as_buildnum%/0.0/g" \
            -e "s/%as_console_jar%/389-admin.jar/g" \
            -e "s/%as_port%/9830/g" \
            -e "s/%as_user%/$USER/g" \
            -e "s/%as_addr%/127.0.0.1/g" \
            -e "s,%admpw%,testtmp/admpw,g" \
            -e "s,%as_error%,testtmp/error,g" \
            -e "s,%as_access%,testtmp/access,g" \
            -e "s,%as_pid%,testtmp/pid,g" \
            -e "s,%as_help_path%,testtmp,g" $file | ldapmodify -x -h $hostname -p $port -D "$rootdn" -w "$rootpw" -a -c
    done
fi

rm -rf testtmp
mkdir testtmp

if [ "$usessl" ] ; then
    ldapurl="ldaps://$hostname:$secport/o=NetscapeRoot"
else
    ldapurl="ldap://$hostname:$port/o=NetscapeRoot"
fi

cat > testtmp/adm.conf <<EOF
ldapurl:    $ldapurl
ldapHost:   $hostname
ldapPort:   $port
sie:   cn=admin-serv-$instance,cn=389 Administration Server,cn=Server Group,cn=$hostname,ou=$domain,o=NetscapeRoot
isie:   cn=389 Administration Server,cn=Server Group,cn=$hostname,ou=$domain,o=NetscapeRoot
port:   9830
ldapStart:   slapd-$instance/start-slapd
securitydir:	$secdir
EOF

cat > testtmp/admpw <<EOF
admin:{SHA}0DPiKuNIrrVmD8IUCuw1hQxNqZc=
EOF

pwpfile=/tmp/pwp.$$
PASSWORD_PIPE=0 ; export PASSWORD_PIPE
cat > $pwpfile <<EOF
User: admin
Password: $adminpw

UserDN: uid=admin,ou=Administrators,ou=TopologyManagement,o=NetscapeRoot
SIEPWD: $adminpw
EOF

dir=`pwd`

cat > .gdbinit <<EOF
break main
run $dir/testtmp $secdir < $pwpfile
EOF

VALGRIND="valgrind --tool=memcheck --leak-check=yes --suppressions=/share/scripts/valgrind.supp --num-callers=40 --log-file=/var/tmp/vg.out"
#GDB="gdb -x .gdbinit "

NOSSLTESTS="retrieveSIE psetread psetwrite"
SSLTESTS="retrieveSIEssl psetreadssl"
if [ "$usessl" ] ; then
    TESTS="$SSLTESTS"
else
    TESTS="$NOSSLTESTS"
fi
for test in $TESTS ; do
    if [ -n "$GDB" ] ; then
        ./libtool --mode execute $GDB ./$test
    else
        if [ -n "$VALGRIND" ] ; then
            VGLOG=".$test"
        fi
        cat $pwpfile | ./libtool --mode execute ${VALGRIND}$VGLOG ./$test $dir/testtmp $secdir
    fi
done

rm -f $pwpfile .gdbinit
