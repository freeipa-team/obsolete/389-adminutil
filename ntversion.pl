# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation version
# 2.1 of the License.
#                                                                                  
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#                                                                                  
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# END COPYRIGHT BLOCK
@date = localtime();
$FILEVERSION = &doubleDigit( $date[5] ) . ',' .		# year
    &doubleDigit( $date[4] + 1 ) . ',' .			# month
    &doubleDigit( $date[3] ) . ',' .				# date
    &doubleDigit( $date[2] );
$FILEVERSIONTEXT = '"' . &doubleDigit( $date[4] ) . '/' .	# month
    &doubleDigit( $date[3] ) . '/' .				# date
    &doubleDigit( $date[5] ) . ' ' .				# year
    &doubleDigit( $date[2] ) . ':' .				# hour
    &doubleDigit( $date[1] ) . '"';				# minute
$MAJORVERSION=$ARGV[1];
$MINORVERSION=$ARGV[2];

open(VERSIONFILE, ">$ARGV[0]/netsite/include/nt/ntversion.h"); 
print VERSIONFILE "// This file is automatically generated.\r\n";
print VERSIONFILE "// Please do not edit this file manually.\r\n";
print VERSIONFILE "// It contains the version number of this build.\r\n";
print VERSIONFILE "\r\n";
print VERSIONFILE "#define VERSION $FILEVERSION\r\n";
print VERSIONFILE "#define PRODUCT $MAJORVERSION,0,0,$MINORVERSION\r\n";
print VERSIONFILE "#define VERSIONTEXT $FILEVERSIONTEXT\r\n";
print VERSIONFILE "#define PRODUCTTEXT \"$MAJORVERSION.$MINORVERSION\"\r\n";
close( VERSIONFILE );

sub doubleDigit {
    $_[0] > 9 ? $_[0] : '0' . $_[0];
}
