/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
#include "libadminutil/resource.h"
#define resource_key(a,b)   a b

#define RESOURCE_FILE PACKAGE_NAME


  /*extracted from errRpt.c*/
#define DBT_errRpt_FILE_ERROR       resource_key(RESOURCE_FILE, "1")
#define DBT_errRpt_MEMORY_ERROR 	resource_key(RESOURCE_FILE, "2")
#define DBT_errRpt_SYSTEM_ERROR 	resource_key(RESOURCE_FILE, "3")
#define DBT_errRpt_INCORRECT_USAGE 	resource_key(RESOURCE_FILE, "4")
#define DBT_errRpt_ELEM_MISSING 	resource_key(RESOURCE_FILE, "5")
#define DBT_errRpt_REGISTRY_DATABASE_ERROR 			resource_key(RESOURCE_FILE, "6" )
#define DBT_errRpt_NETWORK_ERROR 	resource_key(RESOURCE_FILE, "7")
#define DBT_errRpt_GENERAL_FAILURE 	resource_key(RESOURCE_FILE, "8")
#define DBT_errRpt_WARNING 			resource_key(RESOURCE_FILE, "9")
#define DBT_errRpt_APP_ERROR 		resource_key(RESOURCE_FILE, "10")

  /*extracted from pset.c */
#define DBT_pset_OP_OK 			    resource_key(RESOURCE_FILE, "21")
#define DBT_pset_OP_FAIL 			resource_key(RESOURCE_FILE, "22")
#define DBT_pset_SYSTEM_ERR 		resource_key(RESOURCE_FILE, "23")
#define DBT_pset_ENV_ERR 			resource_key(RESOURCE_FILE, "24")
#define DBT_pset_ARGS_ERROR 		resource_key(RESOURCE_FILE, "25")
#define DBT_pset_NULL_HANDLE 		resource_key(RESOURCE_FILE, "26")
#define DBT_pset_LOCAL_MODE 		resource_key(RESOURCE_FILE, "27")
#define DBT_pset_LOCAL_OPEN_FAIL 	resource_key(RESOURCE_FILE, "28")
#define DBT_pset_AUTH_FAIL 			resource_key(RESOURCE_FILE, "29")
#define DBT_pset_ACCESS_FAIL 		resource_key(RESOURCE_FILE, "30")
#define DBT_pset_ENTRY_NOT_EXIST 	resource_key(RESOURCE_FILE, "31")
#define DBT_pset_ATTR_NOT_EXIST 	resource_key(RESOURCE_FILE, "32")
#define DBT_pset_ENTRY_EXIST 		resource_key(RESOURCE_FILE, "33")
#define DBT_pset_ATTR_EXIST 		resource_key(RESOURCE_FILE, "34")
#define DBT_pset_NOT_ENTRY 			resource_key(RESOURCE_FILE, "35")
#define DBT_pset_NOT_ATTR 			resource_key(RESOURCE_FILE, "36")
#define DBT_pset_NO_LDAP_HNDL 		resource_key(RESOURCE_FILE, "37")
#define DBT_pset_NO_DN 			    resource_key(RESOURCE_FILE, "38")
#define DBT_pset_NO_DATA 			resource_key(RESOURCE_FILE, "39")
#define DBT_pset_NO_VALUE 			resource_key(RESOURCE_FILE, "40")
#define DBT_pset_NO_PARENT 			resource_key(RESOURCE_FILE, "41")
#define DBT_pset_PARTIAL_GET 		resource_key(RESOURCE_FILE, "42")
#define DBT_pset_PARTIAL_OP 		resource_key(RESOURCE_FILE, "43")
#define DBT_pset_ILLEGAL_OP 		resource_key(RESOURCE_FILE, "44")
#define DBT_pset_NOT_IMPLEMENT 		resource_key(RESOURCE_FILE, "45")
#define DBT_pset_UNKNOWN_ERROR_NO       resource_key(RESOURCE_FILE, "46")
#define DBT_pset_ATTR_NOT_ALLOWED       resource_key(RESOURCE_FILE, "47")

  /*extracted from form_post.c  */
#define DBT_formPost_Browser_err 	resource_key(RESOURCE_FILE, "61")
#define DBT_formPost_Browser_errDetail 	resource_key(RESOURCE_FILE, "62")
#define DBT_formPost_PostStdinErr 	resource_key(RESOURCE_FILE, "63")
#define DBT_formPost_BadWildcard 	resource_key(RESOURCE_FILE, "64")
#define DBT_formPost_BadWildcardDetail1 	resource_key(RESOURCE_FILE, "65")
#define DBT_formPost_BadWildcardDetail2 	resource_key(RESOURCE_FILE, "66")
#define DBT_formPost_BadReqExp 		resource_key(RESOURCE_FILE, "67")
#define DBT_formPost_BadReqDetail 	resource_key(RESOURCE_FILE, "68")

  /*extracted from  distadm.c */
#define DBT_distadm_pipeErr 		resource_key(RESOURCE_FILE, "81")
#define DBT_distadm_pipeErrDetail 	resource_key(RESOURCE_FILE, "82")

