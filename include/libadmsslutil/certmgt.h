/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/*
 * certmgt.h
 *
 * $Id: certmgt.h,v 1.3 2007/04/04 19:37:46 rmeggins Exp $
 */
#ifndef __certmgt_h
#define __certmgt_h

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Description (certmgt.h)
 *
 *	This file describes the interface to a set functions used by
 *	admin server CGI programs to manage client certificate to login
 *	name mappings.
 */

#include "cert.h"
#include "certdb.h"

/* Functions in certmgt.c */
PR_EXPORT(char *) cmgHTMLCertInfo(CERTCertificate *cert);
PR_EXPORT(CERTSignedCrl *) cmgFindCrlByName(CERTCertDBHandle *handle, char *name, int list_type);
PR_EXPORT(char *) cmgHTMLCrlInfo(CERTSignedCrl *crl);
PR_EXPORT(int) cmgShowCrls(CERTCertDBHandle *handle, int list_type);

#ifdef __cplusplus
}
#endif

#endif /* __certmgt_h */
