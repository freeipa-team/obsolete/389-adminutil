/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/

/*
   The following defines are used by the Win dll version info resource
   files libadminutil/dllVersionINfo.rc and libadmsslutil/dllVersionInfo.rc
 */

#ifdef XP_WIN32
#include <winver.h>
#endif

#define ADMSDK_NAME "Administration Utility"
#define ADMSDK_VMAJOR 1
#define ADMSDK_VMINOR 0
#define ADMSDK_VERSION    "1.0"
#define ADMSDK_SO_VERSION "10"

#ifdef MCC_DEBUG
#define ADMSDK_PRODUCT ADMSDK_VERSION ": " ADMSDK_NAME " (debug)"
#define ADMSDK_FLAGS VS_FF_DEBUG
#else
#define ADMSDK_PRODUCT ADMSDK_VERSION ": " ADMSDK_NAME
#define ADMSDK_FLAGS 0x0L
#endif

#define VI_COMPANYNAME "Red Hat, Inc."
#define VI_COPYRIGHT   "Copyright (C) 2005 Red Hat, Inc."
