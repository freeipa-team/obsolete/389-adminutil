/*
 * BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK
 */
/*
 * psetwrite.c: test program to test the pset API
 * Usage: psetwrite configdir
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libadminutil/psetc.h"
#include "libadminutil/admutil.h"
#include "libadminutil/distadm.h"
#include "libadminutil/srvutil.h"

static void
dump_pset_node(PsetHndl pset, char *nodeName)
{
    char buf[BUFSIZ];
    int rval = 0;
    AttributeList iter;
    AttributePtr attrPtr = NULL;
    AttributeList nodeAttrs = psetGetAllAttrsACI(pset, nodeName, &rval);

    fprintf(stderr, "pset operation returned [%d: %s]\n", rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
    if (!nodeAttrs) {
        goto localdone;
    }

    iter = nodeAttrs;
    while ((attrPtr = *iter++)) {
        char *p = NULL;
        int ii = 0;
        for (p = attrPtr->attrVal[0]; attrPtr->attrVal && attrPtr->attrVal[ii];
             p = attrPtr->attrVal[++ii]) {
            fprintf(stderr, "%s: %s\n", attrPtr->attrName, p);
        }            
    }

localdone:
    deleteAttributeList(nodeAttrs);

    return;
}

int
main(int ac, char **av)
{
    int _ai=ADMUTIL_Init();
    PsetHndl pset = NULL;
    char *configdir = NULL;
    int rval = 0;
    AttributeList updateList = NULL;
    char *attrs[] = {
        "configuration.nsAdminCacheLifetime",
        "configuration.nsAdminAccessHosts",
        "configuration.nsAdminAccessAddresses",
        "configuration.nsAdminEnableEnduser",
        "configuration.nsAdminEnableDSGW"
    };
    int nattrs = sizeof(attrs)/sizeof(attrs[0]);
    char *newattrs[] = {
        "configuration.attribute1",
        "configuration.attribute2",
        "configuration.attribute3",
        "configuration.attribute4",
        "configuration.attribute5"
    };
    int nnewattrs = sizeof(newattrs)/sizeof(newattrs[0]);
    int ii = 0;
    ValueType nodeObjectClass = NULL;
    ValueType ocList = NULL;
    char buf[BUFSIZ];

    (void)_ai;
    if (ac > 1) {
        configdir = strdup(*(av+1));
    }
    
    if (NULL == configdir || 0 == strlen(configdir)) {
        fprintf(stderr, "ERROR: config dir was not specified.\n");
        rval = -1;
        goto done;
    } else {
        fprintf(stdout, "%s: configdir: %s\n", *av, configdir);
    }

    pset = psetCreate(NULL, configdir, NULL, NULL, &rval);
    fprintf(stderr, "pset operation returned [%d: %s]\n", rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
    if (!pset || rval) {
        goto done;
    }

    dump_pset_node(pset, "configuration");

    updateList = createAttributeList(nattrs);
    while (ii < nattrs) {
        addSingleValueAttribute(updateList, ii, attrs[ii], "new value");
        ++ii;
    }
    rval = psetSetAttrList(pset, updateList);
    if (rval) {
        fprintf(stderr, "PSET_ERROR_NUMBER: %d\nPSET_ERROR_INFO: %s\n", 
                rval,
                psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
        goto done;
    }
    deleteAttributeList(updateList);
    updateList = NULL;

    nodeObjectClass = psetGetObjectClass(pset, "configuration", &rval);
    if (rval) {
        psetDelete(pset);
        fprintf(stderr, "PSET_ERROR_NUMBER: %d\nPSET_ERROR_INFO: %s\n", 
                rval,
                psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
        exit(1);
    }

    ii = 0;
    while (nodeObjectClass[ii]) ++ii;
    ocList = createAttrNameList(ii+1);
    for (ii = 0; nodeObjectClass[ii]; ++ii) {
        addName(ocList, ii, nodeObjectClass[ii]);
    }
    deleteValue(nodeObjectClass);
    nodeObjectClass = NULL;
    addName(ocList, ii, "extensibleObject");

    rval = psetSetAttr(pset, "configuration.objectclass", ocList);
    deleteAttrNameList(ocList);
    ocList = NULL;
    if (rval) {
        fprintf(stderr, "PSET_ERROR_NUMBER: %d\nPSET_ERROR_INFO: %s\n", 
                rval,
                psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
        goto done;
    }

    fprintf(stderr, "After adding objectclass extensibleObject: \n");
    dump_pset_node(pset, "configuration");

    ii = 0;
    while (ii < nnewattrs) {
        rval = psetAddSingleValueAttribute(pset, newattrs[ii], "new value");
        if (rval) {
            fprintf(stderr, "Could not add attribute [%s]: PSET_ERROR_NUMBER: %d\nPSET_ERROR_INFO: %s\n",
		    newattrs[ii],
                    rval,
                    psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
            goto done;
        }
        ++ii;
    }

    fprintf(stderr, "After adding bogus attributes\n");
    dump_pset_node(pset, "configuration");

done:
    free(configdir);
    psetDelete(pset);
    deleteValue(nodeObjectClass);
    deleteAttributeList(updateList);
    deleteAttrNameList(ocList);
    exit(rval);
}
