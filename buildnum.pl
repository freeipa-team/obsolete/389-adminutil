#!/usr/bin/perl
# BEGIN COPYRIGHT BLOCK
# Copyright (C) 2005 Red Hat, Inc.
# All rights reserved.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation version
# 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# END COPYRIGHT BLOCK
#--------------------------------------------
# buildnum.pl
#
# Generates a dated build number and writes
# out a buildnum.dat file in a user specified
# subdirectory.
#
# Usage: buildnum.pl -p <platform dir>
#--------------------------------------------

use Getopt::Std;
use FileHandle;
                                                                                             
autoflush STDERR 1;
                                                                                             
getopts('p:H');
                                                                                             
if ($opt_H) {exitHelp();}

# Load arguments
$platdir = $opt_p;

# Get current time
@now = gmtime;

# Format buildnum as YYYY.DDD.HHMM
$year = $now[5] + 1900;
$doy = $now[7] + 1;
if ($doy < 100) { $doy = 0 . $doy; }
$tod = $now[2] . $now[1];
$buildnum = "$year.$doy.$tod";

if ($platdir) {
    # Write buildnum.dat
    $buildnum_file = "./$platdir/buildnum.dat";
    open(BUILDNUM,">$buildnum_file") || die "Error: Can't create $buildnum_file: $!\n";
    print BUILDNUM "\\\"$buildnum\\\"";
    close(BUILDNUM);
} else {
    print "\\\"$buildnum\\\"";
}

#---------- exitHelp subroutine ----------
sub exitHelp {
    print(STDERR "$0: Generates a dated build number.

    \tUsage: $0 -p <platform>

    \t-p <platform>             Platform subdirectory.
    \t-H                        Print this help message\n");
    exit(0);
}
