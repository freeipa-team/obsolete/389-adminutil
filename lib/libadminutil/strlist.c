/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/* 
 * strlist.c:  Managing a handle to a list of strings
 *            
 * All blame to Mike McCool
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libadminutil/admutil.h>


#if defined(NSPR20)
#ifndef MALLOC
#define MALLOC PR_Malloc
#endif

#ifndef CALLOC
#define CALLOC PR_Calloc
#endif

#ifndef REALLOC
#define REALLOC PR_Realloc
#endif

#ifndef FREE
#define FREE PR_Free
#endif

#else /* NSPR20 */

/* For memory allocation/destroy  */
#ifndef MALLOC
#define MALLOC malloc
#endif

#ifndef CALLOC
#define CALLOC calloc
#endif

#ifndef REALLOC
#define REALLOC realloc
#endif

#ifndef FREE
#define FREE free
#endif

#endif /* NSPR20 */

/*
#include "netsite.h"
#include <libadmin/libadmin.h>
*/

PR_IMPLEMENT(char **)
new_strlist(int size)
{
  char **new_list;
  register int x;

  new_list = (char **) MALLOC((size+1)*(sizeof(char *)));
  if (NULL == new_list) {
    return NULL;
  }
  /* <= so we get the one right after the given size as well */
  for(x=0; x<= size; x++)  
    new_list[x] = NULL;

  return new_list;
}

PR_IMPLEMENT(char **)
grow_strlist(char **strlist, int newsize)
{
  char **ans;

  ans = (char **) REALLOC(strlist, (newsize+1)*sizeof(char *));

  return ans;
}

PR_IMPLEMENT(void)
free_strlist(char **strlist)
{
  int x;

  if (NULL == strlist)
    return;

  for(x=0; (strlist[x]); x++) free_str(strlist[x]);
  FREE(strlist);
}


PR_IMPLEMENT(char *)
new_str(int size)
{
  return (char*)MALLOC((size+1));
}


PR_IMPLEMENT(void)
free_str(char *str)
{
  if (str) FREE(str);
}
