%define           nspr_version 4.6
%define           nss_version 3.11
%define           svrcore_version 4.0
%define           mozldap_version 6.0
%define           icu_version 3.4

Summary:          Utility library for directory server administration
Name:             adminutil
Version:          1.1.8
Release:          1%{?dist}
License:          LGPLv2
URL:              http://directory.fedoraproject.org/wiki/AdminUtil
Group:            Development/Libraries
BuildRoot:        %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:    nspr-devel >= %{nspr_version}
BuildRequires:    nss-devel >= %{nss_version}
BuildRequires:    svrcore-devel >= %{svrcore_version}
BuildRequires:    mozldap-devel >= %{mozldap_version}
BuildRequires:    libicu-devel >= %{icu_version}
BuildRequires:    icu >= %{icu_version}

Source0:          http://directory.fedoraproject.org/sources/%{name}-%{version}.tar.bz2

%description
%{name} is libraries of functions used to administer directory
servers, usually in conjunction with the admin server.  %{name} is
broken into two libraries - libadminutil contains the basic
functionality, and libadmsslutil contains SSL versions and wrappers
around the basic functions.  The PSET functions allow applications to
store their preferences and configuration parameters in LDAP, without
having to know anything about LDAP.  The configuration is cached in a
local file, allowing applications to function even if the LDAP server
is down.  The other code is typically used by CGI programs used for
directory server management, containing GET/POST processing code as
well as resource handling (ICU ures API).

%package devel
Summary:  Development and header files for %{name}
Group:    Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: pkgconfig
Requires: nspr-devel >= %{nspr_version}
Requires: nss-devel >= %{nss_version}
Requires: svrcore-devel >= %{svrcore_version}
Requires: mozldap-devel >= %{mozldap_version}
Requires: libicu-devel >= %{icu_version}

%description devel
Development files and header files necessary to build applications
that use %{name}.

%prep
%setup -q

%build

%configure --disable-tests
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf $RPM_BUILD_ROOT

%{__make} install DESTDIR=$RPM_BUILD_ROOT INSTALL="%{__install} -p"
%{__rm} -f $RPM_BUILD_ROOT%{_libdir}/lib*.a
%{__rm} -f $RPM_BUILD_ROOT%{_libdir}/lib*.la

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LICENSE README NEWS
%{_libdir}/*.so.*
%{_datadir}/%{name}

%files devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/*.so
%{_includedir}/libadminutil
%{_includedir}/libadmsslutil

%changelog
* Tue Mar 31 2009 Rich Megginson <rmeggins@redhat.com> - 1.1.8-1
- this is the 1.1.8 release

* Fri Jun 22 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.2-1
- Updated version to 1.1.2
- This version fixes some memory leaks and invalid memory use
- issues

* Wed May 23 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.1-3
- more fedora review stuff - use macros consistently
- make sure install preserves timestamps
- use lgpl instead of gpl for acclanglist.c
- fix undefined weak symbols in libadmsslutil

* Fri May 18 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.1-2
- pkgconfig is a requires not a build requires

* Thu May 17 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.1-1
- Many bug fixes - bumped version to 1.1.1
- fixed concerns from Fedora package review

* Wed Mar 28 2007 Rich Megginson <rmeggins@redhat.com> - 1.1.0-1
- Initial version - based largely on svrcore.spec
