/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/*
 * disadm.h
 *
 * $Id: distadm.h,v 1.5 2007/07/05 21:12:54 nkinder Exp $
 */
#ifndef __DISTADM_H__
#define __DISTADM_H__

#include <nspr.h>
#include <libadminutil/admutil.h>

#ifdef __cplusplus
extern "C" {
#endif

/* This is a copy from safs/cgi.h */
/* MLM - admin server / libadmin need to define some strings */
#ifndef ADM_NO_VALUE_STRING
#define ADM_NO_VALUE_STRING "NS-value-is-null"
#endif
#ifndef ADM_USER_STRING
#define ADM_USER_STRING "User: "
#endif
#ifndef ADM_PASS_STRING
#define ADM_PASS_STRING "Password: "
#endif
#ifndef ADM_AUTH_STRING
#define ADM_AUTH_STRING "Authorization: "
#endif
#ifndef ADM_USERDN_STRING
#define ADM_USERDN_STRING "UserDN: "
#endif
#ifndef ADM_SIEPWD_STRING
#define ADM_SIEPWD_STRING "SIEPWD: "
#endif
#ifndef ADM_NOT_INITIALIZED
#define ADM_NOT_INITIALIZED "NS-not-initialized"
#endif

/* Initialize libadminutil.  Should be called by EVERY CGI. */
/* util.c */
PR_IMPLEMENT(int) ADMUTIL_Init(void);

/* Initialize distributed admin permissions.  Returns zero on success,
 * nonzero on error. */
/* distadm.c */
PR_IMPLEMENT(int) ADM_InitializePermissions(int *errcode);
 
/* Get the current username, taken via pipe from admin server.
 * May return NULL.  Returns zero on success, nonzero on error.  */
/* distadm.c */
PR_IMPLEMENT(int) ADM_GetCurrentUsername(int *errcode, char **username);
 
/* Get the current password, taken via pipe from admin server.
 * May return NULL.  Returns zero on success, nonzero on error.  */
/* distadm.c */
PR_IMPLEMENT(int) ADM_GetCurrentPassword(int *errcode, char **password);
 
/* Get the authorization string, as sent exactly by the client,
 * taken via pipe from admin server.
 * May return NULL.  Returns zero on success, nonzero on error.  */
/* distadm.c */
PR_IMPLEMENT(int) ADM_GetAuthorizationString(int *errcode, char **authstr);
 
/* Get the user DN string, as returned by ACL code from LDAP server,
 * taken via pipe from admin server.
 * May return NULL.  Returns zero on success, nonzero on error.  */
/* distadm.c */
PR_IMPLEMENT(int) ADM_GetUserDNString(int *errcode, char **userdn);
 
#ifdef __cplusplus
}
#endif

#endif /* __DISTADM_H__ */
