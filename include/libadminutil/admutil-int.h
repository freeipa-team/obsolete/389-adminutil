/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/*
 * These interfaces are private to adminutil - not exposed to the public
 * api, but shared among the files in the adminutil libraries
 */
#ifndef __ADMUTIL_INT_H__
#define __ADMUTIL_INT_H__

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <nspr.h>
#include <libadminutil/admutil.h>

struct _AdmldapHdnl;
typedef struct _AdmldapHdnl *AdmldapHdnlPtr;

char*
dn2AttrName(char* dn, char* rootDN);

char*
attrName2dn(char* attrName, char* rootDN);

/*
 * LDAPMod related utility functions
 */
LDAPMod*
createSingleValMod( char* namep, char* value, int mode);

LDAPMod*
createMod( char* namep, ValueType values, int mode);

void
deleteMod(LDAPMod* mod);

void
deleteMods(LDAPMod** mods);

/*
 * List related data structure and method
 */
typedef struct _ListNode {
  char* name;
  void* val;
  int   dflag;
  struct _ListNode *next;
} ListNode, *ListNodePtr;

/*
 * Binary Tree related data structure and methods
 */
typedef struct _TreeNode {
  char* name;
  ListNodePtr val;
  struct _TreeNode *left, *right;
} TreeNode, *TreeNodePtr;

ListNodePtr
createListNode(char* name, void *val, int dflag);

ListNodePtr
listFindNode(ListNodePtr list, char* name);

void*
listGetValue(ListNodePtr list, char* name);

int
listCount(ListNodePtr list);

ListNodePtr
listCat(ListNodePtr list1, ListNodePtr list2);

ListNodePtr
listAppend(ListNodePtr list, char* name, void* val,int dflag);

ListNodePtr
listAdd(ListNodePtr list, char* name, void* val, int dflag);

ListNodePtr
listDelete(ListNodePtr list, char* name);

void
listDestroy(ListNodePtr list);

/* Some utilities which use list */
AttributeList
nvlistConvert(ListNodePtr list);

void
nvlistDestroy(ListNodePtr list);

ListNodePtr
createUpdateList(AttributeList nvl);

void
destroyUpdateList(ListNodePtr);


TreeNodePtr
createTreeNode(char* name, char* val);

int
treeCount(TreeNodePtr root);

TreeNodePtr
treeFindNode(TreeNodePtr node, char* name);

ValueType
treeFindValue(TreeNodePtr root, char* name);

char*
treeFindValueAt(TreeNodePtr root, char* name, int index);

TreeNodePtr
treeAddNode(TreeNodePtr node, TreeNodePtr newNode);

TreeNodePtr
treeAddValue(TreeNodePtr node, char* val);

TreeNodePtr
treeAddNameValue(TreeNodePtr node, char* name, char* val);

int
treeRemoveNode(TreeNodePtr node, char* name, int* removeFlag);

void
treeRemoveTree(TreeNodePtr node);

ListNodePtr
treeBuildAttrList(char* nodeName, TreeNodePtr node);

void
treeExport(FILE *fstream, char* parentString, TreeNodePtr node);

TreeNodePtr
treeImport(FILE *fstream, int* errorcode);

/*
 * utility function for copy/free string array
 */
PR_IMPLEMENT(char**)
admutil_strsdup(char** orig);

PR_IMPLEMENT(char**)
admutil_strsdup_berval(struct berval** orig);

PR_IMPLEMENT(void)
admutil_strsFree(char** target);

int admutil_ldap_url_parse(const char *url, LDAPURLDesc **ludpp, int require_dn, int *secure);
LDAP *admutil_ldap_init(
    AdmldapInfo info,
    const char *ldapurl, /* full ldap url */
    const char *hostname, /* can also use this to override
			     host in url */
    int port, /* can also use this to override port in url */
    int secure, /* 0 for ldap, 1 for ldaps */
    int shared, /* if true, LDAP* will be shared among multiple threads */
    const char *filename /* for ldapi */
);
int admutil_ldap_bind(
    LDAP *ld, /* ldap connection */
    const char *bindid, /* usually a bind DN for simple bind */
    const char *creds, /* usually a password for simple bind */
    const char *mech, /* name of mechanism */
    LDAPControl **serverctrls, /* additional controls to send */
    LDAPControl ***returnedctrls, /* returned controls */
    struct timeval *timeout, /* timeout */
    int *msgidp /* pass in non-NULL for async handling */
);

char **admldap_ldap_explode_dn( const char *dn, const int notypes );
char **admldap_ldap_explode_rdn( const char *rdn, const int notypes );
#endif  /* __ADMUTIL_INT_H__ */
