/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
#ifndef __PSETC_PVT_H__
#define __PSETC_PVT_H__

#include <ldap.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libadminutil/psetc.h>
#include "libadminutil/admutil.h"
#include "libadminutil/admutil-int.h"

/*
 * PsetNode related data structure and methods
 */
typedef struct _PsetNode {

  char        *attrName;

  /* LDAP related */
  LDAPMessage *attrLDAP;
  LDAPMessage *ldapHolder;

  ListNodePtr ldapDumpYard;

  /* File related */
  TreeNodePtr  attrFile;

  /* child LDAP entries */
  ListNodePtr  children;
} PsetNode, *PsetNodePtr;

PsetNodePtr
psetNodeCreate(char* attrName, LDAPMessage *ldapEntry);

void
psetNodeDestroy(PsetNodePtr target);

int
psetNodeAddNameValue(PsetNodePtr target, char* name, char* val);

int
psetNodeAddNameMultiValue(PsetNodePtr target, char* name, char** vals);

int
psetNodeModNameValue(PsetNodePtr target, char* name, char* val);

int
psetNodeModNameMultiValue(PsetNodePtr target, char* name, char** vals);

int
psetNodeDelAttr(PsetNodePtr target, char* name);

int
psetNodeAddChild(PsetNodePtr target, PsetNodePtr child);

int
psetNodeRemoveChild(PsetNodePtr target, char*  childName);

AttrNameList
psetNodeGetChildren(PsetNodePtr target, int* errorcode);

int
psetNodeSetLDAP(PsetNodePtr target, LDAPMessage *entry);

PsetNodePtr
psetNodeFindNode(PsetNodePtr nodePtr, LDAP *ld, char *name, int* nodeFlag, 
		 int* errorcode);

ValueType
psetNodeFindValue(PsetNodePtr nodePtr, LDAP *ld, char *name, int* errorcode);

ListNodePtr
psetNodeGetAll(PsetNodePtr nodePtr, LDAP *ld, int deep, int* errorcode);

PsetNodePtr
psetNodeBuildNodes(PsetNodePtr nodePtr, char *nodeName, int* errorcode);

PsetNodePtr
psetNodeFindParent(PsetNodePtr nodePtr, LDAP *ld, char *name, int* errorcode);

int
psetNodeLDAPUpdate(PsetNodePtr nodePtr, LDAP *ld, char* ldFilter, LDAPMod **mods);

int
psetNodeFileUpdate(PsetNodePtr nodePtr, AttributeList nvl);

int
psetNodeCount(PsetNodePtr nodePtr);

int
psetNodeFileExport(PsetNodePtr nodePtr, LDAP *ld, FILE *fstream);


/*
 * Pset related data structure and private methods 
 */
typedef struct _Pset {
  /* LDAP Handler */
  LDAP         *ld;
  int          ldunbindf;
  char*        ldapFilter;

  /* DN */
  char*         sieDN;

  /* USER related info, for referal */
  char*         binddn;
  char*         bindpw;

  /* info for file */
  char*         configFile;
  int           fileRW;  /* 0: ReadOnly, 1: ReadWrite  */

  /* real info */
  PsetNodePtr   info;

  AdmldapInfo   admInfo;

} Pset, *PsetPtr;


int
psetLDAPRefresh(PsetPtr pset);

int
psetFileImportP(PsetPtr pset);

int
psetFileExportP(PsetPtr pset);


typedef struct _PsetRoot {

  PsetPtr   ldapInfo;
  PsetPtr   *fileInfo;

} PsetRoot, *PsetRootPtr;

PsetNodePtr
psetRootFindNode(PsetRootPtr psetRoot, NameType name, PsetPtr* pset, 
		 int* nodeFlag, int* errorcode);

int
psetRootModAttribute(PsetRootPtr psetRoot, int mode, NameType name, ValueType val);

int
psetRootModAttrList(PsetRootPtr psetRoot, int mode, AttributeList nvl);


/* For pset duplication  */
int
psetRecursiveReplicate(PsetHndl source, PsetHndl dest, NameType name, 
		       char *source_groupDN, char *dest_groupDN);

void 
replace_dn(char **attribute_list, char *source_groupDN, char *dest_groupDN);

#endif  /* __PSETC_PVT_H__ */
