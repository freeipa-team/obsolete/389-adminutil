/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/*
 * admsslutil.h
 *
 * $Id: admsslutil.h,v 1.4 2007/05/08 19:13:25 rmeggins Exp $
 */

#ifndef __ADMSSLUTIL_H__
#define __ADMSSLUTIL_H__

#include "libadminutil/admutil.h"
#include "libadminutil/psetc.h"
#include "prio.h"
#include "prerror.h"

#ifdef __cplusplus
extern "C" {
#endif

PR_IMPLEMENT(int)
admldapBuildInfoSSL(AdmldapInfo info,  int *errorcode);

PR_IMPLEMENT(int)
admldapSetAdmGrpUserDirectoryCGI(AdmldapInfo info,
				 char* directoryURL,
				 char* bindDN, 
				 char* bindPassword,
				 char* directoryInfoRef, 
				 int* error_code);

PR_IMPLEMENT(int)
admldapSetDomainUserDirectoryCGI(AdmldapInfo info,
				 char* directoryURL,
				 char* bindDN, 
				 char* bindPassword,
				 char* directoryInfoRef, 
				 int* error_code);


/* force means init NSS even if SSL is not being used - for hashing, etc. */
PR_IMPLEMENT(int)
ADMSSL_Init(AdmldapInfo info, char *securitydir, int force);

/* force means init NSS even if SSL is not being used - for hashing, etc. */
PR_IMPLEMENT(int)
ADMSSL_InitSimple(char *configdir, char *securitydir, int force);

char *ADM_GetPassword(char *prompt);

#ifdef XP_WIN32
char *ADM_GetPassword_wHelp(char *prompt, const char *helpURL);
#endif

void set_security(PsetHndl pset, 
		  char *configdir, /* where config files may be found */
          char *security); /* on or off */

void _conf_setdefaults(void);
char *_conf_setciphers(char *ciphers);

PRStatus SSLPLCY_Install(void);

const char *SSL_Strerror(PRErrorCode errNum);

PR_IMPLEMENT(PRFileDesc*)
SSLSocket_init(PRFileDesc *req_socket, const char *configdir, const char *securitydir);

#ifdef __cplusplus
}
#endif


#endif /* __ADMSSLUTIL_H__ */
