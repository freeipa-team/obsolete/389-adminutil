/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/*
 * resource.h
 *
 * $Id: resource.h,v 1.4 2008/12/22 18:55:20 rmeggins Exp $
 */
#ifndef _RESOURCE_H_
#define _RESOURCE_H_

#include "libadminutil/admutil.h"

#ifdef __cplusplus
extern "C" {
#endif


/*******************************************************************************/

/* Resource contains the name of the
   property file w/ paht information
*/
typedef struct
{
	char *path;
	char *package;
    void *propset;
} Resource;

/* Initialization routine.  Checks for the existence of a resourcebundle
   with the default encoding -  

   path: relative directory path to the properties file (ex, "server/property")
         which is set by res_init_path
   package: name of the properties file. (ex, "myresource")

   return value:  NULL if any memory allocation fails
				  NULL if the property file cannot be located
				  Resource* when successful
*/
PR_IMPLEMENT(Resource*)
res_init_resource(const char* path, const char* package);

/*  This routine retrieves a string from a property file that was initialized 
	to source.  It tries to locate a property file according to the 
	acceptlanguage.  

    source: pointer to Resource returned from initialization
	key: nueric key value.  Note than enum is generated from dbt*.h files.
	     For example, if you have Resdef(my_key, 1, "this is a test") in dbt*.h
		 file for your source, your key will be my_key.  

    If buffer is NULL, the return value will be allocated by malloc and must be free'd by
    the caller.  Even in an error condition, a malloc'd empty string will be returned.
    The value will be returned in buffer, if given.  buffer will be properly NULL terminated,
    even if bufsize is not large enough to accomodate the entire string (i.e. it's truncated).
    Buffer will always be NULL terminated, so that even if an error occurred, buffer will be
    initialized to an empty string, so you do not have to worry about initializing it first.
    If buffer is given, the return value will point to buffer, so that you can use the return
    value directly:
    char buf[BUFSIZE];
    int rc = 0;
    ...
    fprintf(stderr, "Error: %s\n",
    	    res_getstring(source, MY_ERROR_KEY, lang, buf, sizeof(buf), &rc));

    The rc parameter may be used to determine if there was an overflow condition or some other error.
    If rc == 0, the operation was successful.  If rc == 1, an overflow occurred - the given buffer was
    too small to hold the contents.  If rc == -1 or some other value, some other error occurred, 
*/
PR_IMPLEMENT(char*) 
res_getstring(Resource* source, char *key, char *accept_language, char *buffer, size_t bufsize, int *rc);

/* frees Resource* from initialization routine.
*/
PR_IMPLEMENT(void)
res_destroy_resource(Resource* to_destroy);

/*
  ----------------------------------------------------------------
  res_find_and_init_resource

  Initializes a property file path.  Looks for the package directory
  in a variety of well known locations, in order, and stops after
  the first successful attempt to stat the directory.
  1) the given path, if any
  2) the current working directory + "/property"
  3) getenv("ADMINUTIL_CONF_DIR") + "/property"
  It is expected that applications will have their default property
  directory compiled in (via configure ; make) and that's what they
  will pass in as their first argument.  The other path lookup stuff
  is really for legacy apps or apps in which the user wants to change
  the property directory at runtime.  The package argument may be
  NULL, if path is already package specific e.g. /usr/share/adminutil,
  in which case path should contain the .res files.
  -----------------------------------------------------------------
 */
PR_IMPLEMENT(Resource*)
res_find_and_init_resource(const char *path, const char *package);

/********************/
/* XP_AccLangList() */
/********************/

#define MAX_ACCEPT_LANGUAGE 16
#define MAX_ACCEPT_LENGTH 18

typedef char ACCEPT_LANGUAGE_LIST[MAX_ACCEPT_LANGUAGE][MAX_ACCEPT_LENGTH];

/* Given an AcceptLanguage string in the HTTP_ACCEPT_LANGUAGE format, return
   an array of languages, sorted by the quality values (if any).  If the given
   string is empty, the list will consist of one value, "en", the default language.
*/
PR_EXTERN( int )
XP_AccLangList(char* AcceptLanguage,
               ACCEPT_LANGUAGE_LIST AcceptLanguageList);


#ifdef __cplusplus
}
#endif


/*******************************************************************************/
/* 
 * this table contains library name
 * (stored in the first string entry, with id=0),
 * and the id/string pairs which are used by library  
 */

typedef struct res_RESOURCE_TABLE
{
  int id;
  char *str;
} res_RESOURCE_TABLE;

/*******************************************************************************/

/* 
 * resource global contains resource table list which is used
 * to generate the database.
 * Also used for "in memory" version of XP_GetStringFromDatabase()
 */

typedef struct res_RESOURCE_GLOBAL
{
  res_RESOURCE_TABLE  *restable;
} res_RESOURCE_GLOBAL;

/*******************************************************************************/

/*
 * Define the ResDef macro to simplify the maintenance of strings which are to
 * be added to the library or application header file (dbtxxx.h). This enables
 * source code to refer to the strings by theit TokenNames, and allows the
 * strings to be stored in the database.
 *
 * Usage:   ResDef(TokenName,TokenValue,String)
 *
 * Example: ResDef(DBT_HelloWorld_, \
 *                 1,"Hello, World!")
 *          ResDef(DBT_TheCowJumpedOverTheMoon_, \
 *                 2,"The cow jumped over the moon.")
 *          ResDef(DBT_TheValueOfPiIsAbout31415926536_, \
 *                 3,"The value of PI is about 3.1415926536."
 *
 * RESOURCE_STR is used by makstrdb.c only.  It is not used by getstrdb.c or
 * in library or application source code.
 */
 
#ifdef  RESOURCE_STR
#define BEGIN_STR(argLibraryName) \
    res_RESOURCE_TABLE argLibraryName[] = { {0, #argLibraryName} ,
#define ResDef(argToken,argID,argString) \
    {argID, argString},
#define END_STR(argLibraryName) \
    {0, 0} };
#else
#define BEGIN_STR(argLibraryName) \
                          enum {
#define ResDef(argToken,argID,argString) \
                          argToken = argID,
#define END_STR(argLibraryName) \
                          argLibraryName ## top };
#endif



#endif /* _RESOURCE_H_ */
