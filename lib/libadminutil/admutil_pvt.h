/** BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2001 Sun Microsystems, Inc.  Used by permission.
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 *                                                                                  
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *                                                                                  
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK **/
/*
 * These interfaces are private to admutil.c - these interfaces should
 * not be used by other files
 * For interfaces implemented by admutil that are to be shared internally
 * among files in adminutil, but should not be part of the public api,
 * use admutil-int.h
 */
#ifndef __ADMUTIL_PVT_H__
#define __ADMUTIL_PVT_H__

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libadminutil/admutil.h>
#include "libadminutil/admutil-int.h"

/*
 * utility function for compare timestamp
 */
int
timecmp(char* ldapTime, time_t *cacheTime);


/*
 * Utility for convert dn to/from attribute name
 */

/*
 * AdmldapInfo Data
 */
typedef struct _AdmldapHdnl {
  char         *configFilePath;
  TreeNodePtr  configInfo;
  char         *serverDirectoryURL;
  LDAPURLDesc  *ldapInfo;
  LDAP         *ldapHndl;
  char         *admpwFilePath;
  char         *localAdminName;
  char         *localAdminPassword;
  char         *sieDN;
  char         *userDN;
  char         *passwd;
  char         *authDN;
  char         *authPasswd;
  int          secure;
} AdmldapHdnl;

#endif  /* __ADMUTIL_PVT_H__ */
