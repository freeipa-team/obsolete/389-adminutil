/*
 * BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK
 */
/*
 * psetreadssl.c: test program to test the pset read apis
 * Usage: psetreadssl configdir
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libadminutil/distadm.h"
#include "libadminutil/admutil.h"
#include "libadminutil/srvutil.h"
#include "libadmsslutil/psetcssl.h"
#include "libadmsslutil/admsslutil.h"

#include <nss.h>
#include <ssl.h>

static void
dump_pset_node(PsetHndl pset, char *nodeName)
{
    char buf[BUFSIZ];
    int rval = 0;
    AttributeList iter;
    AttributePtr attrPtr = NULL;
    AttributeList nodeAttrs = psetGetAllAttrsACI(pset, nodeName, &rval);

    fprintf(stderr, "pset operation returned [%d: %s]\n", rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
    if (!nodeAttrs) {
        goto localdone;
    }

    iter = nodeAttrs;
    while ((attrPtr = *iter++)) {
        char *p = NULL;
        int ii = 0;
        if (attrPtr->attrVal) {
            for (p = attrPtr->attrVal[0]; attrPtr->attrVal && attrPtr->attrVal[ii];
                 p = attrPtr->attrVal[++ii]) {
                fprintf(stderr, "%s: %s\n", attrPtr->attrName, p);
            }
        }
    }

localdone:
    deleteAttributeList(nodeAttrs);

    return;
}

int
main(int ac, char **av)
{
    int _ai=ADMUTIL_Init();
    PsetHndl pset = NULL;
    char *configdir = NULL;
    char *securitydir = NULL;
    int rval = 0;
    char buf[BUFSIZ];

    (void)_ai;
    if (ac > 1) {
        configdir = strdup(*(av+1));
    }
    
    if (ac > 2) {
        securitydir = strdup(*(av+2));
    }
    
    if (NULL == configdir || 0 == strlen(configdir)) {
        fprintf(stderr, "ERROR: config dir was not specified.\n");
        rval = -1;
        goto done;
    } else {
        fprintf(stdout, "%s: configdir: %s\n", *av, configdir);
    }

    if (NULL == securitydir || 0 == strlen(securitydir)) {
        fprintf(stderr, "ERROR: security dir was not specified.\n");
        rval = -2;
        goto done;
    } else {
        fprintf(stdout, "%s: securitydir: %s\n", *av, securitydir);
    }

    rval = ADMSSL_InitSimple(configdir, securitydir, 0);
    if (rval) {
        fprintf(stderr, "ADMSSL_InitSimple(%s,%s) failed: %d\n", configdir, securitydir, rval);
        goto done;
    }

    pset = psetCreateSSL("slapd-ds", configdir, NULL, NULL, &rval);
    fprintf(stderr, "pset operation returned [%d: %s]\n", rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
    if (!pset || (rval && (rval != PSET_LOCAL_OPEN_FAIL))) { /* no local file for slapd - OK */
        goto done;
    }

    dump_pset_node(pset, "");
    dump_pset_node(pset, "configuration");

    psetDelete(pset);
    pset = psetCreateSSL(NULL, configdir, NULL, NULL, &rval); /* admin-serv */
    fprintf(stderr, "pset operation returned [%d: %s]\n", rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
    if (!pset || rval) { /* no local file for slapd - OK */
        goto done;
    }

    dump_pset_node(pset, "");
    dump_pset_node(pset, "configuration");

done:
    free(configdir);
    free(securitydir);
    psetDelete(pset);
    SSL_ClearSessionCache();
    NSS_Shutdown();
    PR_Cleanup();
    exit(rval);
}
