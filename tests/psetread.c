/*
 * BEGIN COPYRIGHT BLOCK
 * Copyright (C) 2005 Red Hat, Inc.
 * All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version
 * 2.1 of the License.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * END COPYRIGHT BLOCK
 */
/*
 * retireveSIE.c: test program to test an API getServerDNList
 * Usage: retireveSIE configdir
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libadminutil/admutil.h"
#include "libadminutil/distadm.h"
#include "libadminutil/srvutil.h"

static void
dump_pset_node(PsetHndl pset, char *nodeName)
{
    char buf[BUFSIZ];
    int rval = 0;
    AttributeList iter;
    AttributePtr attrPtr = NULL;
    AttributeList nodeAttrs = psetGetAllAttrsACI(pset, nodeName, &rval);

    fprintf(stderr, "pset operation returned [%d: %s]\n", rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
    if (!nodeAttrs) {
        goto localdone;
    }

    iter = nodeAttrs;
    while ((attrPtr = *iter++)) {
        char *p = NULL;
        int ii = 0;
        if (!attrPtr->attrVal) {
          fprintf(stderr, "%s: no values\n", attrPtr->attrName);
          continue;
        }
        for (p = attrPtr->attrVal[0]; attrPtr->attrVal && p;
             p = attrPtr->attrVal[++ii]) {
            fprintf(stderr, "%s: %s\n", attrPtr->attrName, p);
        }            
    }

localdone:
    deleteAttributeList(nodeAttrs);

    return;
}

int
main(int ac, char **av)
{
    int _ai=ADMUTIL_Init();
    AttrNameList nameList = NULL;
    AdmldapInfo ldapInfo = NULL;
    PsetHndl pset = NULL;
    char *configdir = NULL;
    int rval = 0;
    char buf[BUFSIZ];
    char fname[BUFSIZ];
    char *host;
    char *userdn;
    char *passwd;
    char **p;
    int ii = 0;

    (void)_ai;
    if (ac > 1) {
        configdir = strdup(*(av+1));
    }
    
    if (NULL == configdir || 0 == strlen(configdir)) {
        fprintf(stderr, "ERROR: config dir was not specified.\n");
        rval = -1;
        goto done;
    } else {
        fprintf(stdout, "%s: configdir: %s\n", *av, configdir);
    }

    /*
     * get the LDAP information from admin server config info
     */
    ldapInfo = admldapBuildInfo(configdir, &rval);
    if (NULL == ldapInfo) {
        fprintf(stderr, "ERROR: empty ldapInfo\n");
        rval = 1;
        goto done;
    }

    if (ADM_GetUserDNString(&rval, &userdn) || !userdn) {
        fprintf(stderr, "ERROR: could not get current userdn %d\n", rval);
        goto done;
    }
    /* override - cannot use siedn any more for auth */
    admldapSetSIEDN(ldapInfo, userdn);

    if (ADM_GetCurrentPassword(&rval, &passwd)) {
      fprintf(stderr, "ERROR: could not get current password %d\n", rval);
      goto done;
    }

    host = admldapGetHost(ldapInfo);
    /*
     * get servers' DNs belonging to the Admin Server
     */
    nameList = getServerDNList(ldapInfo);
    for (p = nameList; p && *p; p++) {
      PR_snprintf(fname, sizeof(fname), "%s/%d.1.pset", configdir, ii);
      pset = psetRealCreate(ldapInfo, host, admldapGetPort(ldapInfo),
                            *p, userdn, passwd, fname, &rval);
      fprintf(stderr, "pset operation returned [%d: %s]\n", rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
      if (!pset || rval) {
        deleteAttrNameList(nameList);
        PL_strfree(host);
        goto done;
      }

      dump_pset_node(pset, "");
      dump_pset_node(pset, "configuration");

      psetDelete(pset);

      PR_snprintf(fname, sizeof(fname), "%s/%d.2.pset", configdir, ii);
      pset = psetRealCreate(ldapInfo, host, admldapGetPort(ldapInfo),
                            "cn=config", userdn, passwd, fname, &rval);
      fprintf(stderr, "pset operation returned [%d: %s]\n", rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
      if (!pset || rval) {
        deleteAttrNameList(nameList);
        PL_strfree(host);
        goto done;
      }

      dump_pset_node(pset, "");
      dump_pset_node(pset, "configuration");

      psetDelete(pset);
      ++ii;
    }
    PL_strfree(host);
    pset = psetCreate(NULL, configdir, NULL, NULL, &rval); /* admin-serv */
    fprintf(stderr, "pset operation returned [%d: %s]\n", rval, psetErrorString(rval, NULL, buf, sizeof(buf), NULL));
    if (!pset || (rval && (rval != PSET_LOCAL_OPEN_FAIL))) { /* no local file for slapd - OK */
        goto done;
    }

    dump_pset_node(pset, "");
    dump_pset_node(pset, "configuration");

done:
    psetDelete(pset);
    free(configdir);
    exit(rval);
}
